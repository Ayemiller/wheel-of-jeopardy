package jhu.softwareengineering.wheelofjeopardy.gui;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.control.LabeledMatchers.hasText;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import javafx.application.Platform;
import javafx.stage.Stage;

public class WheelOfJeopardyStageTest extends ApplicationTest {

  private WheelOfJeopardyStage app;


  @Test
  public void a_Stage() {
    // expect:
    verifyThat("Play Game with default questions", hasText("Play Game with default questions"));
    verifyThat("Play Game with custom questions", hasText("Play Game with custom questions"));
  }

  // @Test
  public void b_savePlayerStage() throws InterruptedException {
    // when:
    clickOn("Play Game");
    Thread.sleep(1000);
    // then:
    verifyThat("Save Players", hasText("Save Players"));
  }

  @Override
  public void start(final Stage stage) throws Exception {
    Platform.runLater(() -> {
      app = new WheelOfJeopardyStage();
      app.start(stage);
    });
  }

  @Override
  public void stop() {}

  @Test
  public void test_one() {
    assertTrue(true);


  }

}

