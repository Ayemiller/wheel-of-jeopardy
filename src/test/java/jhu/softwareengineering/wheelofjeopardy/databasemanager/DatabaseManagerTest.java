package jhu.softwareengineering.wheelofjeopardy.databasemanager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import com.google.common.eventbus.EventBus;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.WheelSectors;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.WheelSectors.WheelSector;
import jhu.softwareengineering.wheelofjeopardy.gui.JeopardyQuestionResponseToGuiMessage;
import jhu.softwareengineering.wheelofjeopardy.gui.RequestJeopardyQuestionFromDatabaseMessage;

class DatabaseManagerTest {
  private DatabaseManager db;
  private EventBus eventBus;

  @BeforeEach
  public void setup() {
    eventBus = Mockito.mock(EventBus.class);
    db = new DatabaseManager(eventBus);
  }

  @Test
  void testHandleQuestionRequest() {
    WheelSectors.setName(WheelSector.CATEGORY_1, "Astrology");
    final String categoryName = WheelSectors.getName(WheelSector.CATEGORY_1);
    final RequestJeopardyQuestionFromDatabaseMessage request =
        new RequestJeopardyQuestionFromDatabaseMessage(categoryName, 100, 1);
    db.handleQuestionRequest(request);
    final ArgumentCaptor<JeopardyQuestionResponseToGuiMessage> argumentCaptor =
        ArgumentCaptor.forClass(JeopardyQuestionResponseToGuiMessage.class);
    Mockito.verify(eventBus).post(argumentCaptor.capture());
  }

}
