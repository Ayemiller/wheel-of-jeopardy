package jhu.softwareengineering.wheelofjeopardy.databasemanager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.google.common.eventbus.EventBus;

class DbConnectionTest {
  private DatabaseManager db;
  private DbConnection dbConnection;
  private EventBus eventBus;

  @BeforeEach
  public void setup() {
    eventBus = Mockito.mock(EventBus.class);
    db = new DatabaseManager(eventBus);
    db.startUp("questions-and-answers.csv", 1);
    dbConnection = db.dbConnection;
  }

  @Test
  void testCategoryLoading() {
    assertEquals("GoT", dbConnection.getQuestionCategories(1).get(0));
    assertEquals("animals", dbConnection.getQuestionCategories(1).get(1));
    assertEquals("cheese", dbConnection.getQuestionCategories(1).get(2));
    assertEquals("fruit", dbConnection.getQuestionCategories(1).get(3));
    assertEquals("shapes", dbConnection.getQuestionCategories(1).get(4));
    assertEquals("sports", dbConnection.getQuestionCategories(1).get(5));
  }

  @Test
  void testGetQuestionFromCategory() {
    final JeopardyQuestion fixture = dbConnection.getQuestionFromCategory("GoT", 100, 1);
    assertEquals("Tyrion Lannister", fixture.getAnswer());
    assertEquals("Which character said \"a mind needs books like a sword needs a whetstone\"",
        fixture.getQuestion());
    assertEquals("GoT", fixture.getCategory());
    assertEquals(100, fixture.getPointValue());
  }

  @Test
  void testGetQuestionFromCategoryException() {
    final JeopardyQuestion fixture =
        dbConnection.getQuestionFromCategory("Category Does Not Exist", 100, 1);
    assertEquals(null, fixture);
  }

  @Test
  void testInsertQuestionAnswer() {
    final QaLoader qaLoader = new QaLoader();
    qaLoader.insertQuestionAnswer("Question?", "Answer!", "new category", 100000, 1);
    final List<JeopardyQuestion> questions = dbConnection.getAllJeopardyQuestion();
    assertEquals(61, questions.size());
  }

  @Test
  void testNumberOfQuestions() {
    final List<JeopardyQuestion> questions = dbConnection.getAllJeopardyQuestion();
    assertEquals(60, questions.size());
    assertEquals("apple.jpg", questions.get(0).getQuestion());
    assertEquals("apple", questions.get(0).getAnswer());
    assertEquals(100, questions.get(0).getPointValue());
  }

  @Test
  void testPrintQuestionAnswers() {
    assertEquals(true, dbConnection.printQuestionAnswers());
  }

}
