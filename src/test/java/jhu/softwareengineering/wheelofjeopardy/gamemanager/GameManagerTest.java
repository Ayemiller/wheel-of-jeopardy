package jhu.softwareengineering.wheelofjeopardy.gamemanager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import com.google.common.eventbus.EventBus;

class GameManagerTest {

  private GameManager fixture;
  protected int endRoundMessageCount;
  protected int spinResultMessageCount;

  @Test
  void endCurrentPlayersTurn() {

    final EventBus eventBus = Mockito.mock(EventBus.class);
    endRoundMessageCount = 0;
    spinResultMessageCount = 0;
    Mockito.doAnswer(new Answer<Void>() {
      @Override
      public Void answer(final InvocationOnMock invocationOnMock) throws Throwable {
        final Object argument = invocationOnMock.getArguments()[0];

        if (argument instanceof EndRoundMessage) {
          endRoundMessageCount++;
        } else if (argument instanceof AlertGuiOfSpinResultMessage) {
          spinResultMessageCount++;
        } else {
          throw new UnsupportedOperationException("This kind of event was not expected");
        }
        return null;
      }
    }).when(eventBus).post(ArgumentMatchers.any());
    fixture = new GameManager(eventBus);
    fixture.createPlayers("bacon1", "bacon2");
    assertEquals(2, fixture.getPlayers().size());
    assertEquals("bacon1", fixture.getCurrentPlayer().getName());
    fixture.endCurrentPlayersTurn();
    assertEquals("bacon2", fixture.getCurrentPlayer().getName());
    assertEquals(50, fixture.getSpinsRemaining());
    for (int i = 1; i < 50; i++) {
      final GuiPerformedSpinRequestMessage spinRequest =
          new GuiPerformedSpinRequestMessage(fixture.getCurrentPlayer());
      fixture.handleSpinRequest(spinRequest);
      fixture.endCurrentPlayersTurn();
      assertEquals(50 - i, fixture.getSpinsRemaining());
    }
    assertEquals(49, spinResultMessageCount);
    assertEquals(0, endRoundMessageCount);

    final GuiPerformedSpinRequestMessage spinRequest =
        new GuiPerformedSpinRequestMessage(fixture.getCurrentPlayer());
    fixture.handleSpinRequest(spinRequest);
    fixture.endCurrentPlayersTurn();
    // next round triggered, therefore, 50 spins remaining
    assertEquals(50, fixture.getSpinsRemaining());
    assertEquals(1, endRoundMessageCount);

  }

  @Test
  void testGetPlayers() {
    fixture = new GameManager(Mockito.mock(EventBus.class));
    assertEquals(0, fixture.getPlayers().size());
    fixture.createPlayers("Foo", "Bar");
    assertEquals(2, fixture.getPlayers().size());
    assertEquals("Foo", fixture.getCurrentPlayer().getName());
  }

  @Test
  void testStartGameManager() {
    fixture = new GameManager(Mockito.mock(EventBus.class));
    assertEquals(1, fixture.getRound());
    assertEquals(50, fixture.getSpinsRemaining());
    final GuiPerformedSpinRequestMessage spinRequest =
        new GuiPerformedSpinRequestMessage(new Player("Player 1"));

    fixture.handleSpinRequest(spinRequest);
    assertEquals(49, fixture.getSpinsRemaining());

    for (int i = 1; i < 50; i++) {
      fixture.handleSpinRequest(spinRequest);
      assertEquals(49 - i, fixture.getSpinsRemaining());
    }
    // zero spins left
    assertEquals(0, fixture.getSpinsRemaining());
    fixture.handleSpinRequest(spinRequest);
    // now spins are reset
    assertEquals(50, fixture.getSpinsRemaining());

    // resuming spins decrementing
    fixture.handleSpinRequest(spinRequest);
    assertEquals(49, fixture.getSpinsRemaining());
  }

}
