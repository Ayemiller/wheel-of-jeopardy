package jhu.softwareengineering.wheelofjeopardy.gamemanager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.WheelSectors.WheelSector;

class WheelSectorsTest {
  @BeforeEach
  void setUp() {
    WheelSectors.wheelSectors.remove(WheelSectors.WheelSector.CATEGORY_1);
    WheelSectors.wheelSectors.remove(WheelSectors.WheelSector.CATEGORY_2);
    WheelSectors.wheelSectors.remove(WheelSectors.WheelSector.CATEGORY_3);
    WheelSectors.wheelSectors.remove(WheelSectors.WheelSector.CATEGORY_4);
    WheelSectors.wheelSectors.remove(WheelSectors.WheelSector.CATEGORY_5);
    WheelSectors.wheelSectors.remove(WheelSectors.WheelSector.CATEGORY_6);
  }

  @Test
  void testGetCategories() {
    // since all Jeopardy categories have been removed, 0 Jeopardy categories are expected
    assertEquals(0, WheelSectors.getJeopardyCategories().size());

    // adding 1 Jeopardy category
    WheelSectors.wheelSectors.put(WheelSectors.WheelSector.CATEGORY_1, "test");
    assertEquals(1, WheelSectors.getJeopardyCategories().size());

    // removing that Jeopardy category
    WheelSectors.wheelSectors.remove(WheelSectors.WheelSector.CATEGORY_1);
    assertEquals(0, WheelSectors.getJeopardyCategories().size());
  }

  @Test
  void testRandomSectorRetrieval() {
    assertTrue(WheelSectors.getRandomSector() instanceof WheelSector);
  }

  @Test
  /**
   * The actual use-case for this method involves removing a Jeopardy-question category, but using a
   * non-Jeopardy-question category to test demonstrates the functionality
   */
  void testRemoveCategory() {

    assertEquals(6, WheelSectors.wheelSectors.size());
    WheelSectors.removeCategory("bankrupt");
    assertEquals(5, WheelSectors.wheelSectors.size());

    WheelSectors.removeCategory("doesn't-exist");
    assertEquals(5, WheelSectors.wheelSectors.size());

    WheelSectors.removeCategory("lose-turn");
    assertEquals(4, WheelSectors.wheelSectors.size());
    WheelSectors.removeCategory("double-score");
    assertEquals(3, WheelSectors.wheelSectors.size());
    WheelSectors.removeCategory("opponent's-choice");
    assertEquals(2, WheelSectors.wheelSectors.size());
    WheelSectors.removeCategory("player's-choice");
    assertEquals(1, WheelSectors.wheelSectors.size());
    WheelSectors.removeCategory("free-turn");
    assertEquals(0, WheelSectors.wheelSectors.size());

    WheelSectors.setName(WheelSector.CATEGORY_1, "rando");
    assertEquals(1, WheelSectors.wheelSectors.size());
  }
}
