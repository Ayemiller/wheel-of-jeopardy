package jhu.softwareengineering.wheelofjeopardy.gamemanager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.WheelSectors.WheelSector;

/**
 * Message class to alert the GUI to the result of a spin from a given Player.
 */
public class AlertGuiOfSpinResultMessageTest {

  private static final String PLAYER_1 = "Player 1";

  private AlertGuiOfSpinResultMessage fixture;

  Player player;

  @BeforeEach
  public void setup() {
    player = new Player(PLAYER_1);

  }

  @Test
  void testBankruptFixture() {
    fixture = new AlertGuiOfSpinResultMessage(player, WheelSector.BANKRUPT);
    assertEquals(player, fixture.getPlayer());
    assertEquals(PLAYER_1, fixture.getPlayerName());
    assertEquals(WheelSector.BANKRUPT, fixture.getWheelSector());
  }

  @Test
  void testCategory1Fixture() {
    fixture = new AlertGuiOfSpinResultMessage(player, WheelSector.CATEGORY_1);
    assertEquals(player, fixture.getPlayer());
    assertEquals(PLAYER_1, fixture.getPlayerName());
    assertEquals(WheelSector.CATEGORY_1, fixture.getWheelSector());
    assertTrue(fixture.isJeopardyQuestion());
  }

  @Test
  void testCategory2Fixture() {
    fixture = new AlertGuiOfSpinResultMessage(player, WheelSector.CATEGORY_2);
    assertEquals(player, fixture.getPlayer());
    assertEquals(PLAYER_1, fixture.getPlayerName());
    assertEquals(WheelSector.CATEGORY_2, fixture.getWheelSector());
    assertTrue(fixture.isJeopardyQuestion());
  }

  @Test
  void testCategory3Fixture() {
    fixture = new AlertGuiOfSpinResultMessage(player, WheelSector.CATEGORY_3);
    assertEquals(player, fixture.getPlayer());
    assertEquals(PLAYER_1, fixture.getPlayerName());
    assertEquals(WheelSector.CATEGORY_3, fixture.getWheelSector());
    assertTrue(fixture.isJeopardyQuestion());
  }

  @Test
  void testCategory4Fixture() {
    fixture = new AlertGuiOfSpinResultMessage(player, WheelSector.CATEGORY_4);
    assertEquals(player, fixture.getPlayer());
    assertEquals(PLAYER_1, fixture.getPlayerName());
    assertEquals(WheelSector.CATEGORY_4, fixture.getWheelSector());
    assertTrue(fixture.isJeopardyQuestion());
  }

  @Test
  void testCategory5Fixture() {
    fixture = new AlertGuiOfSpinResultMessage(player, WheelSector.CATEGORY_5);
    assertEquals(player, fixture.getPlayer());
    assertEquals(PLAYER_1, fixture.getPlayerName());
    assertEquals(WheelSector.CATEGORY_5, fixture.getWheelSector());
    assertTrue(fixture.isJeopardyQuestion());
  }

  @Test
  void testCategory6Fixture() {
    fixture = new AlertGuiOfSpinResultMessage(player, WheelSector.CATEGORY_6);
    assertEquals(player, fixture.getPlayer());
    assertEquals(PLAYER_1, fixture.getPlayerName());
    assertEquals(WheelSector.CATEGORY_6, fixture.getWheelSector());
    assertTrue(fixture.isJeopardyQuestion());
  }

  @Test
  void testNotQuestionFixture() {
    fixture = new AlertGuiOfSpinResultMessage(player, WheelSector.DOUBLE_YOUR_SCORE);
    assertEquals(player, fixture.getPlayer());
    assertEquals(PLAYER_1, fixture.getPlayerName());
    assertEquals(WheelSector.DOUBLE_YOUR_SCORE, fixture.getWheelSector());
    assertFalse(fixture.isJeopardyQuestion());
  }

}
