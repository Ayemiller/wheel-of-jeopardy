package jhu.softwareengineering.wheelofjeopardy.gamemanager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import com.google.common.eventbus.EventBus;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.WheelSectors.WheelSector;

class PlayerTest {
  private GameManager fixture;

  @BeforeEach
  public void setup() {
    fixture = new GameManager(Mockito.mock(EventBus.class));
    fixture.createPlayers("player1", "player2");
  }

  @Test
  void testAddExtraTurn() {
    assertFalse(fixture.getCurrentPlayer().hasExtraTurns());
    fixture.getCurrentPlayer().addExtraTurn();
    assertTrue(fixture.getCurrentPlayer().hasExtraTurns());
  }

  @Test
  void testAdjustRoundOneScore() {
    fixture.getCurrentPlayer().adjustScore(100, 1);
    assertEquals(100, fixture.getCurrentPlayer().getRoundScore(1));
    fixture.getCurrentPlayer().adjustScore(-200, 1);
    assertEquals(-100, fixture.getCurrentPlayer().getRoundScore(1));
  }

  @Test
  void testAdjustRoundTwoScore() {
    fixture.getCurrentPlayer().adjustScore(400, 2);
    assertEquals(400, fixture.getCurrentPlayer().getRoundScore(2));
    fixture.getCurrentPlayer().adjustScore(-800, 2);
    assertEquals(-400, fixture.getCurrentPlayer().getRoundScore(2));
  }

  @Test
  void testDoubleScoreRoundOne() {
    fixture.getCurrentPlayer().adjustScore(400, 1);
    fixture.getCurrentPlayer().doubleScore(1);
    assertEquals(800, fixture.getCurrentPlayer().getRoundScore(1));
  }

  @Test
  void testDoubleScoreRoundTwo() {
    fixture.getCurrentPlayer().adjustScore(-800, 2);
    fixture.getCurrentPlayer().doubleScore(2);
    assertEquals(-1600, fixture.getCurrentPlayer().getRoundScore(2));
  }

  @Test
  void testGetFinalScore() {
    fixture.getCurrentPlayer().adjustScore(500, 1);
    fixture.getCurrentPlayer().adjustScore(1500, 2);
    assertEquals(2000, fixture.getCurrentPlayer().getFinalScore());
  }

  @Test
  void testGoesBankruptRoundOne() {
    fixture.getCurrentPlayer().adjustScore(10000, 1);
    assertEquals(10000, fixture.getCurrentPlayer().getRoundScore(1));
    fixture.getCurrentPlayer().adjustScore(20000, 2);
    assertEquals(20000, fixture.getCurrentPlayer().getRoundScore(2));
    fixture.getCurrentPlayer().goesBankrupt(1);
    assertEquals(0, fixture.getCurrentPlayer().getRoundScore(1));
    assertEquals(20000, fixture.getCurrentPlayer().getRoundScore(2));
    assertEquals(20000, fixture.getCurrentPlayer().getFinalScore());
  }

  @Test
  void testGoesBankruptRoundTwo() {
    fixture.getCurrentPlayer().adjustScore(10000, 1);
    assertEquals(10000, fixture.getCurrentPlayer().getRoundScore(1));
    fixture.getCurrentPlayer().adjustScore(20000, 2);
    assertEquals(20000, fixture.getCurrentPlayer().getRoundScore(2));
    fixture.getCurrentPlayer().goesBankrupt(2);
    assertEquals(10000, fixture.getCurrentPlayer().getRoundScore(1));
    assertEquals(0, fixture.getCurrentPlayer().getRoundScore(2));
    assertEquals(10000, fixture.getCurrentPlayer().getFinalScore());
  }

  @Test
  void testHandleBankruptSpinResponse() {
    fixture.getCurrentPlayer().adjustScore(100, 1);
    fixture.getCurrentPlayer().adjustScore(200, 2);
    assertEquals(100, fixture.getCurrentPlayer().getRoundScore(1));
    assertEquals(200, fixture.getCurrentPlayer().getRoundScore(2));
    assertEquals(300, fixture.getCurrentPlayer().getFinalScore());
    fixture.handleSpinResult(fixture.getCurrentPlayer(), WheelSector.BANKRUPT, 1);
    assertEquals(0, fixture.getCurrentPlayer().getRoundScore(1));
    assertEquals(200, fixture.players.get(0).getRoundScore(2));
    assertEquals(200, fixture.players.get(0).getFinalScore());
  }

  @Test
  void testHandleDoubleYourScoreSpinResponse() {
    fixture.getCurrentPlayer().adjustScore(100, 1);
    fixture.getCurrentPlayer().adjustScore(200, 2);
    assertEquals(100, fixture.getCurrentPlayer().getRoundScore(1));
    assertEquals(200, fixture.getCurrentPlayer().getRoundScore(2));
    assertEquals(300, fixture.getCurrentPlayer().getFinalScore());
    fixture.handleSpinResult(fixture.getCurrentPlayer(), WheelSector.DOUBLE_YOUR_SCORE, 1);
    assertEquals(200, fixture.players.get(0).getRoundScore(1));
    assertEquals(200, fixture.players.get(0).getRoundScore(2));
    assertEquals(400, fixture.players.get(0).getFinalScore());
  }

  @Test
  void testHandleFreeTurnSpinResponse() {
    fixture.getCurrentPlayer().adjustScore(100, 1);
    fixture.getCurrentPlayer().adjustScore(200, 2);
    assertEquals(100, fixture.getCurrentPlayer().getRoundScore(1));
    assertEquals(200, fixture.getCurrentPlayer().getRoundScore(2));
    assertEquals(300, fixture.getCurrentPlayer().getFinalScore());
    assertFalse(fixture.getCurrentPlayer().hasExtraTurns());
    fixture.handleSpinResult(fixture.getCurrentPlayer(), WheelSector.FREE_TURN, 1);
    assertEquals(100, fixture.players.get(0).getRoundScore(1));
    assertEquals(200, fixture.players.get(0).getRoundScore(2));
    assertEquals(300, fixture.players.get(0).getFinalScore());
    assertTrue(fixture.players.get(0).hasExtraTurns());
  }

  @Test
  void testHandlingAnotherPlayersTurn() {
    fixture.getCurrentPlayer().adjustScore(100, 1);
    fixture.getCurrentPlayer().adjustScore(200, 2);
    assertEquals(100, fixture.getCurrentPlayer().getRoundScore(1));
    assertEquals(200, fixture.getCurrentPlayer().getRoundScore(2));
    assertEquals(300, fixture.getCurrentPlayer().getFinalScore());
    assertFalse(fixture.getCurrentPlayer().hasExtraTurns());
    fixture.handleSpinResult(fixture.getCurrentPlayer(), WheelSector.LOSE_TURN, 2);
    fixture.handleSpinResult(fixture.getCurrentPlayer(), WheelSector.BANKRUPT, 2);
    assertEquals(100, fixture.players.get(0).getRoundScore(1));
    assertEquals(0, fixture.players.get(0).getRoundScore(2));
    assertEquals(100, fixture.players.get(0).getFinalScore());
    assertFalse(fixture.players.get(0).hasExtraTurns());
  }

}

