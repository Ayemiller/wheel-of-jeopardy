package jhu.softwareengineering.wheelofjeopardy.gamemanager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Message class that is sent from the Gui to tell the GameManager that the spin button has been
 * pressed.
 */
public class GuiPerformedSpinRequestMessageTest {

  private GuiPerformedSpinRequestMessage fixture;

  Player player;

  @BeforeEach
  public void setup() {
    player = new Player("Player 1");
    fixture = new GuiPerformedSpinRequestMessage(player);
  }

  @Test
  void testGuiPerformedSpinRequestionMessage() {
    assertEquals(player, fixture.getPlayer());
    assertEquals("Player 1", fixture.getPlayerName());
  }

}
