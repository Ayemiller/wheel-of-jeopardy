package jhu.softwareengineering.wheelofjeopardy.gamemanager;

import jhu.softwareengineering.wheelofjeopardy.gamemanager.WheelSectors.WheelSector;

/**
 * Message class to alert the GUI to the result of a spin from a given Player.
 */
public class AlertGuiOfSpinResultMessage {
  private final Player player;
  private final WheelSector wheelSector;

  public AlertGuiOfSpinResultMessage(final Player player, final WheelSector wheelSector) {
    this.player = player;
    this.wheelSector = wheelSector;
  }

  public Player getPlayer() {
    return player;
  }

  public String getPlayerName() {
    return player.getName();
  }

  public WheelSector getWheelSector() {
    return wheelSector;
  }

  public boolean isJeopardyQuestion() {
    return wheelSector.equals(WheelSector.CATEGORY_1) || wheelSector.equals(WheelSector.CATEGORY_2)
        || wheelSector.equals(WheelSector.CATEGORY_3) || wheelSector.equals(WheelSector.CATEGORY_4)
        || wheelSector.equals(WheelSector.CATEGORY_5) || wheelSector.equals(WheelSector.CATEGORY_6);
  }

  public boolean shouldSpinImmedatelyEnd() {
    return wheelSector.equals(WheelSector.FREE_TURN)
        || wheelSector.equals(WheelSector.DOUBLE_YOUR_SCORE)
        || wheelSector.equals(WheelSector.BANKRUPT);
  }

}
