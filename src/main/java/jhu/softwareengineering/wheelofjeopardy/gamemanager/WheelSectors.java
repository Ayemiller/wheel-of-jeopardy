package jhu.softwareengineering.wheelofjeopardy.gamemanager;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ThreadLocalRandom;
import com.google.common.base.Strings;

/**
 * The values for the different sectors of the wheel.
 */
public class WheelSectors {

  public enum WheelSector {
    BANKRUPT, CATEGORY_1, CATEGORY_2, CATEGORY_3, CATEGORY_4, CATEGORY_5, CATEGORY_6, DOUBLE_YOUR_SCORE, FREE_TURN, LOSE_TURN, OPPONENTS_CHOICE, PLAYERS_CHOICE;
  }

  static EnumMap<WheelSector, String> wheelSectors = new EnumMap<>(WheelSector.class);

  static {
    wheelSectors.put(WheelSector.BANKRUPT, "bankrupt");
    wheelSectors.put(WheelSector.DOUBLE_YOUR_SCORE, "double-score");
    wheelSectors.put(WheelSector.FREE_TURN, "free-turn");
    wheelSectors.put(WheelSector.LOSE_TURN, "lose-turn");
    wheelSectors.put(WheelSector.OPPONENTS_CHOICE, "opponent's-choice");
    wheelSectors.put(WheelSector.PLAYERS_CHOICE, "player's-choice");
  }

  public static List<String> getJeopardyCategories() {
    final List<String> jeopardyCategories = new ArrayList<>();
    if (!Strings.isNullOrEmpty(getName(WheelSector.CATEGORY_1))) {
      jeopardyCategories.add(getName(WheelSector.CATEGORY_1));
    }
    if (!Strings.isNullOrEmpty(getName(WheelSector.CATEGORY_2))) {
      jeopardyCategories.add(getName(WheelSector.CATEGORY_2));
    }
    if (!Strings.isNullOrEmpty(getName(WheelSector.CATEGORY_3))) {
      jeopardyCategories.add(getName(WheelSector.CATEGORY_3));
    }
    if (!Strings.isNullOrEmpty(getName(WheelSector.CATEGORY_4))) {
      jeopardyCategories.add(getName(WheelSector.CATEGORY_4));
    }
    if (!Strings.isNullOrEmpty(getName(WheelSector.CATEGORY_5))) {
      jeopardyCategories.add(getName(WheelSector.CATEGORY_5));
    }
    if (!Strings.isNullOrEmpty(getName(WheelSector.CATEGORY_6))) {
      jeopardyCategories.add(getName(WheelSector.CATEGORY_6));
    }
    return jeopardyCategories;
  }

  /**
   * Return the name of a given wheel-sector based on the current database contents.
   *
   * @param wheelSector
   * @return name of the specified wheelSector
   */
  public static String getName(final WheelSector wheelSector) {
    return wheelSectors.get(wheelSector);
  }

  public static WheelSector getRandomSector() {
    final WheelSector[] wheelSectorArray =
        wheelSectors.keySet().toArray(new WheelSector[wheelSectors.keySet().size()]);
    return wheelSectorArray[ThreadLocalRandom.current().nextInt(wheelSectorArray.length)];
  }

  public static void removeCategory(final String wheelSector) {
    for (final Entry<WheelSector, String> entry : wheelSectors.entrySet()) {
      if (entry.getValue().equals(wheelSector)) {
        wheelSectors.remove(entry.getKey());
        break;
      }
    }
  }

  /**
   * Sets the name of a given wheel-sector based on the current database contents.
   *
   * @param wheelSector
   * @param sector's name
   */
  public static void setName(final WheelSector wheelSector, final String name) {
    wheelSectors.put(wheelSector, name);
  }

  static void resetJeopardyCategories() {
    wheelSectors.remove(WheelSector.CATEGORY_1);
    wheelSectors.remove(WheelSector.CATEGORY_2);
    wheelSectors.remove(WheelSector.CATEGORY_3);
    wheelSectors.remove(WheelSector.CATEGORY_4);
    wheelSectors.remove(WheelSector.CATEGORY_5);
    wheelSectors.remove(WheelSector.CATEGORY_6);
  }

  private WheelSectors() { /* intentionally left blank */ }

}
