package jhu.softwareengineering.wheelofjeopardy.gamemanager;

/**
 * Class representing an individual player of WoJ.
 */
public class Player {

  private class RemoveTurnRuntimeException extends RuntimeException {
    private static final long serialVersionUID = -4744767470116191336L;

    RemoveTurnRuntimeException(final String message) {
      super(message);
    }
  }

  private int extraTurns;
  private final String playerName;
  private int roundOneScore;

  private int roundTwoScore;

  public Player(final String name) {
    playerName = name;
    roundOneScore = 0;
    roundTwoScore = 0;
    extraTurns = 0;
  }

  public void addExtraTurn() {
    extraTurns++;
  }

  public void adjustScore(final int scoreUpdate, final int round) {
    // passing a positive scoreUpdate value will increment score
    // passing a negative scoreUpdate value will decrement score
    if (round == 1) {
      roundOneScore += scoreUpdate;
    } else {
      roundTwoScore += scoreUpdate;
    }
  }

  public void doubleScore(final int round) {
    if (round == 1) {
      roundOneScore *= 2;
    } else {
      roundTwoScore *= 2;
    }
  }

  public int getFinalScore() {
    return roundOneScore + roundTwoScore;
  }

  public String getName() {
    return playerName;
  }

  public int getRoundScore(final int round) {
    if (round == 1) {
      return roundOneScore;
    } else {
      return roundTwoScore;
    }
  }

  public void goesBankrupt(final int round) {
    if (round == 1) {
      roundOneScore = 0;
    } else {
      roundTwoScore = 0;
    }
  }

  /**
   * @return whether or not the player has extra turns.
   */
  public boolean hasExtraTurns() {
    return extraTurns > 0;
  }

  public void removeExtraTurn() {
    if (extraTurns == 0) {
      throw new RemoveTurnRuntimeException(
          String.format("Cannot remove extra turn for %s, none exist", playerName));
    }
    extraTurns--;
  }
}
