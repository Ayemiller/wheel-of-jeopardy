package jhu.softwareengineering.wheelofjeopardy.gamemanager;

/**
 * Message class that is sent from the Gui to tell the GameManager that the spin button has been
 * pressed.
 */
public class GuiPerformedSpinRequestMessage {

  Player player;

  public GuiPerformedSpinRequestMessage(final Player player) {
    this.player = player;
  }

  public Player getPlayer() {
    return player;
  }

  public String getPlayerName() {
    return player.getName();
  }

}
