package jhu.softwareengineering.wheelofjeopardy.gamemanager;

public class EndRoundMessage {

  private final int roundNumber;

  public EndRoundMessage(final int roundNumber) {
    this.roundNumber = roundNumber;
  }

  public int getRoundNumber() {
    return roundNumber;
  }

}
