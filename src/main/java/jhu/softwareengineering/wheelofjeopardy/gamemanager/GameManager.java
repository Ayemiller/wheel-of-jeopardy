package jhu.softwareengineering.wheelofjeopardy.gamemanager;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.WheelSectors.WheelSector;
import jhu.softwareengineering.wheelofjeopardy.gui.PlayerAnsweredQuestionResultMessage;

/**
 * GameManager service.
 */
public class GameManager {

  private static final Logger LOGGER = LogManager.getLogger(GameManager.class);
  private static final int SPINS_PER_ROUND = 50;
  private Player currentPlayer;
  private int currentRound = 1;
  private final EventBus eventBus;
  private int spinsRemaining;
  List<Player> players = new ArrayList<>();

  public GameManager(final EventBus eventBus) {
    this.eventBus = eventBus;
    eventBus.register(this);
    startRound(currentRound);
  }

  public void advanceToNextRound() {
    startRound(++currentRound);
  }

  public void createPlayers(final String player1, final String player2) {
    players.add(new Player(player1));
    players.add(new Player(player2));
    currentPlayer = players.get(0);
  }

  public void endCurrentPlayersTurn() {
    for (final Player player : players) {
      if (!player.equals(currentPlayer)) {
        currentPlayer = player;
        break;
      }
    }
    if (spinsRemaining == 0) {
      sendEndRoundMessage();
    }
  }

  public Player getCurrentPlayer() {
    return currentPlayer;
  }

  public List<Player> getPlayers() {
    return players;
  }

  public int getRound() {
    return currentRound;
  }

  public int getSpinsRemaining() {
    return spinsRemaining;
  }

  public Player getWinner() {
    if (players.get(0).getFinalScore() > players.get(1).getFinalScore()) {
      return players.get(0);
    } else {
      return players.get(1);
    }
  }

  @Subscribe
  public void handlePlayerAnsweredQuestionResultMessage(
      final PlayerAnsweredQuestionResultMessage msg) {
    currentPlayer.adjustScore(msg.getPoints(), currentRound);
  }

  @Subscribe
  /**
   * This method handles the message sent from the spin button to inform that the user is spinning
   * the wheel.
   *
   * @param spinRequest
   */
  public void handleSpinRequest(final GuiPerformedSpinRequestMessage spinRequest) {
    LOGGER.info("GameManager received SpinRequest from GUI");
    if (spinsRemaining > 0) {
      spinWheel(spinRequest.getPlayer());
      spinsRemaining--;
    } else {
      LOGGER.info("No spins remaining!");
      sendEndRoundMessage();
    }
  }

  private void resetSpinsRemaining() {
    spinsRemaining = SPINS_PER_ROUND;
  }

  private void sendEndRoundMessage() {
    resetSpinsRemaining();
    eventBus.post(new EndRoundMessage(getRound()));
  }

  /**
   * This method handles a request from the GUI to spin the WoJ wheel. The player's score is
   * adjusted appropriately and an {@link AlertGuiOfSpinResult} message is posted to update the GUI
   * of the result of the spin.
   *
   * @param player
   */
  private void spinWheel(final Player player) {
    final WheelSector spinResult = WheelSectors.getRandomSector();
    handleSpinResult(player, spinResult, currentRound);
  }

  protected void startRound(final int round) {
    LOGGER.info("Starting round {}", round);
    currentRound = round;
    resetSpinsRemaining();
  }


  @VisibleForTesting
  void handleSpinResult(final Player player, final WheelSector spinResult, final int currentRound) {
    final Player spinPlayer = player;
    switch (spinResult) {
      case BANKRUPT:
        spinPlayer.goesBankrupt(currentRound);
        break;
      case DOUBLE_YOUR_SCORE:
        spinPlayer.doubleScore(currentRound);
        break;
      case FREE_TURN:
        spinPlayer.addExtraTurn();
        break;
      case LOSE_TURN:
      case OPPONENTS_CHOICE:
      case PLAYERS_CHOICE:
        break;
      case CATEGORY_1:
      case CATEGORY_2:
      case CATEGORY_3:
      case CATEGORY_4:
      case CATEGORY_5:
      case CATEGORY_6:
        break;
      default:
        LOGGER.error("Unknown spin result.");
        break;
    }
    eventBus.post(new AlertGuiOfSpinResultMessage(spinPlayer, spinResult));
  }

}
