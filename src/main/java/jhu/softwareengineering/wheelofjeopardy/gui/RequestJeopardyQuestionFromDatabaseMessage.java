package jhu.softwareengineering.wheelofjeopardy.gui;

/**
 * Message class sent from the GUI to the database to request a specific question from the Jeopardy
 * Board.
 */
public class RequestJeopardyQuestionFromDatabaseMessage {
  private final int pointValue;
  private final int round;
  private final String wheelSector;

  public RequestJeopardyQuestionFromDatabaseMessage(final String wheelSector, final int pointValue,
      final int round) {
    this.pointValue = pointValue;
    this.wheelSector = wheelSector;
    this.round = round;
  }

  public String getCategory() {
    return wheelSector;
  }

  public int getPointValue() {
    return pointValue;
  }

  public int getRound() {
    return round;
  }

}
