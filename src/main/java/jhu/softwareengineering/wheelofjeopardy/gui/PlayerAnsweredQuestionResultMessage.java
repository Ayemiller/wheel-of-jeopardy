package jhu.softwareengineering.wheelofjeopardy.gui;

import jhu.softwareengineering.wheelofjeopardy.gamemanager.Player;

public class PlayerAnsweredQuestionResultMessage {
  private final Player player;
  private final int points;

  public PlayerAnsweredQuestionResultMessage(final Player player, final int points) {
    this.player = player;
    this.points = points;
  }

  public Player getPlayer() {
    return player;
  }

  public int getPoints() {
    return points;
  }

}
