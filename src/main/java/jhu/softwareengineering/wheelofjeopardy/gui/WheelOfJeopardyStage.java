package jhu.softwareengineering.wheelofjeopardy.gui;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.google.common.base.Strings;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Duration;
import jhu.softwareengineering.wheelofjeopardy.databasemanager.DatabaseManager;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.AlertGuiOfSpinResultMessage;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.EndRoundMessage;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.GameManager;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.GuiPerformedSpinRequestMessage;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.Player;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.WheelSectors;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.WheelSectors.WheelSector;

/**
 * Class to control GUI window with gameplay screens
 *
 * @Author Rachel Szteinberg
 **/
public class WheelOfJeopardyStage extends Application {
  private static final String LARGER_FONT_SIZE = "-fx-font-size: 1.5em; ";
  private static final Logger LOGGER = LogManager.getLogger(WheelOfJeopardyStage.class);
  private static final int SCREEN_HEIGHT = 850;
  private static final int SCREEN_WIDTH = 1150;
  private static final String SMALL_FONT_SIZE = "-fx-font-size: 1.2em; ";
  private static final int TIME_TO_ANSWER = 30;
  private String categorySelected;
  private String correctAnswerFromDatabase;
  private Player currentQuestionPlayer;
  private DatabaseManager databaseManager;
  private EventBus eventBus;
  private GameManager gameManager;
  private JeopardyBoardPane jeopardyBoardPane;
  private int nextValue;
  private String questionAsked;
  private boolean startNewRound = false;
  String databaseFile;

  @Subscribe
  public void handleEndRoundMessage(final EndRoundMessage msg) {
    startNewRound = true;
  }

  @Subscribe
  /**
   * Update the textField with the JeopardyQuestionResponseToGui event
   *
   * @param response
   */
  public void handleJeopardyQuestionResponse(final JeopardyQuestionResponseToGuiMessage response) {

    questionAsked = response.getJeopardyQuestion().getQuestion();
    nextValue = response.getJeopardyQuestion().getPointValue();
    correctAnswerFromDatabase = response.getJeopardyQuestion().getAnswer();
  }

  @Subscribe
  /**
   * This method handles the {@link AlertGuiOfSpinResult} message sent from a player after a spin is
   * performed. If the spinResult is a jeopardy question, a new
   * {@link RequestJeopardyQuestionFromDatabase} is posted to the eventBus.
   *
   * @param spinResult
   */
  public void handleSpinResult(final AlertGuiOfSpinResultMessage spinResult) {
    final WheelSector wheelSector = spinResult.getWheelSector();
    categorySelected = WheelSectors.getName(wheelSector);

    if (spinResult.isJeopardyQuestion()) {
      eventBus.post(new RequestNextJeopardyQuestionScoreFromBoardMessage(
          WheelSectors.getName(wheelSector), gameManager.getRound()));
    } else {
      if (spinResult.shouldSpinImmedatelyEnd()) {
        gameManager.endCurrentPlayersTurn();
      }
      questionAsked = "";
      nextValue = 0;
    }
  }


  /**
   * Method to run Wheel of Jeopardy Application
   */
  @Override
  public void start(final Stage primaryStage) {
    // instantiate class objects and event bus
    eventBus = new EventBus();
    eventBus.register(this);
    gameManager = new GameManager(eventBus);
    databaseManager = new DatabaseManager(eventBus);

    // title the stage
    primaryStage.setTitle("Wheel of Jeopardy");

    // initialize Q&A
    categorySelected = "";
    questionAsked = "";
    correctAnswerFromDatabase = "";
    nextValue = 1000;

    // show game window
    primaryStage.setScene(getHomeScene(primaryStage)); // set the scene on the stage
    primaryStage.setResizable(true); // enable stage resizing
    primaryStage.show(); // show the stage & its scene
  }

  @Override
  public void stop() {
    if (jeopardyBoardPane != null) {
      jeopardyBoardPane.dispose();
    }
  }

  /**
   * Helper method to create the wheel of categories
   *
   * @return wheel group containing sectors and category labels
   */
  private Group createWheel() {

    // create a wheel of categories
    final Group wheel = new Group();

    final List<WheelSector> categoryList = Arrays.asList(WheelSector.values());

    final int center = 750 / 2;
    final float centerF = center;
    final int radius = 750 / 2 - 150;
    final float radiusF = radius;

    final Circle background = new Circle(center, center, radius);
    background.setFill(Color.WHITE);
    wheel.getChildren().add(background);

    // for center 750, placement of text
    final int[] xpos = {200, 250, 300, 390, 460, 500, 500, 425, 390, 305, 220, 190};
    final int[] ypos = {350, 285, 235, 235, 270, 350, 430, 480, 530, 530, 480, 405};
    final int[] angle = {0, 45, 70, 290, 315, 0, 0, 45, 70, 290, 315, 0};

    // create each sector with text label
    for (int count = 1; count <= 12; count++) {

      final float startAngle = 30f * count;
      final float endAngle = startAngle + 30;

      // create sector
      final Arc sector = new Arc(centerF, centerF, radiusF, radiusF, startAngle, endAngle);
      sector.setFill(Color.TRANSPARENT);
      sector.setStroke(Color.BLACK);
      sector.setType(ArcType.ROUND);

      // position category text
      final WheelSector wheelSector = categoryList.get(count - 1);
      final Text category =
          new Text(xpos[count - 1], ypos[count - 1], WheelSectors.getName(wheelSector));
      category.setRotate(angle[count - 1]);


      // add sector and category to wheel group
      wheel.getChildren().add(sector);
      wheel.getChildren().add(category);
    }

    return wheel;
  }


  /**
   * Answer scene to view answer and mark correct or incorrect
   *
   * @param mainStage
   * @return answer scene object
   */
  private Scene getAnswerScene(final Stage mainStage) {
    // create a grid pane layout
    final GridPane rootPane = new GridPane();
    rootPane.setHgap(10);
    rootPane.setVgap(10);
    rootPane.setAlignment(Pos.CENTER);
    rootPane.setStyle(SMALL_FONT_SIZE);

    // text labels
    rootPane.add(new Label("Category: "), 0, 0);
    rootPane.add(new Label(categorySelected), 1, 0);

    rootPane.add(new Label("Value: "), 0, 1);
    rootPane.add(new Label(nextValue + " points"), 1, 1);

    rootPane.add(new Label("Question: "), 0, 2);
    if (questionAsked.endsWith(".jpg")) {
      rootPane.add(new Label("What was that an image of?"), 1, 2);
    } else {
      rootPane.add(new Label(questionAsked), 1, 2);
    }
    rootPane.add(new Label("Answer: "), 0, 3);
    rootPane.add(new Label(correctAnswerFromDatabase), 1, 3);

    // buttons
    final Button correctButton = new Button("Answered Correctly");
    correctButton.setOnAction(e -> {
      final PlayerAnsweredQuestionResultMessage msg =
          new PlayerAnsweredQuestionResultMessage(gameManager.getCurrentPlayer(), nextValue);
      eventBus.post(msg);
      gameManager.endCurrentPlayersTurn();
      mainStage.setScene(getWheelScene(mainStage));
    });
    correctButton.setDefaultButton(true);
    rootPane.add(correctButton, 0, 4);

    final Button wrongButton = new Button("Answered Wrong");
    wrongButton.setOnAction(e -> setPlayerAnsweredIncorrectlyScene(mainStage));
    rootPane.add(wrongButton, 1, 4);

    // return scene
    return new Scene(rootPane, SCREEN_WIDTH, SCREEN_HEIGHT);
  }

  /**
   * End game screen. Lists the winner and their score. Asks the user whether or not they want to
   * play again.
   *
   * @param mainStage
   * @return end game Scene
   */
  private Scene getEndGameScene(final Stage mainStage) {

    final GridPane rootNode = new GridPane();
    rootNode.setHgap(10);
    rootNode.setVgap(10);
    rootNode.setAlignment(Pos.CENTER);
    rootNode.setStyle(SMALL_FONT_SIZE);

    final Label endGameLabel = new Label("Thanks for playing Wheel of Jeopardy.");
    final Label winnerLabel = new Label(String.format("Player %s Wins with %d Points",
        gameManager.getWinner().getName(), gameManager.getWinner().getFinalScore()));
    final Label playAgainLabel = new Label("Play again?");
    final Button playAgainYesButton = new Button("Yes");
    playAgainYesButton.setOnAction(e -> {
      startNewRound = false;
      start(mainStage);
    });
    final Button playAgainNoButton = new Button("No");
    playAgainNoButton.setOnAction(e -> mainStage.close());
    rootNode.add(endGameLabel, 0, 0);
    rootNode.add(winnerLabel, 0, 1);
    rootNode.add(playAgainLabel, 0, 2);
    final GridPane playAgainPane = new GridPane();
    playAgainPane.add(playAgainYesButton, 0, 0);
    playAgainPane.add(playAgainNoButton, 2, 0);
    rootNode.add(playAgainPane, 0, 3);
    return new Scene(rootNode, SCREEN_WIDTH, SCREEN_HEIGHT);
  }

  /**
   * Screen to get file input for question database
   *
   * @param mainStage
   * @return file input scene
   */
  private Scene getFileInputScene(final Stage mainStage) {
    final Label instruction = new Label("Please select the file to use as the question database");

    final TextField fileTF = new TextField();
    fileTF.setMinWidth(500);
    final Label openFiles = new Label("Select Question Database CSV:");

    final FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Select Question CSV File");
    fileChooser.getExtensionFilters().addAll(new ExtensionFilter("CSV Files", "*.csv"));


    final Button openFilesButton = new Button("Browse");
    openFilesButton.setOnAction(e -> {
      fileTF.clear();
      final File selectedFile = fileChooser.showOpenDialog(mainStage);
      if (selectedFile != null) {
        try {
          databaseFile = selectedFile.getCanonicalPath();
        } catch (final IOException ioException) {
          databaseFile = selectedFile.getAbsolutePath();
          LOGGER.error("Unable to return canonical file path for {}", selectedFile.getName());
        }
        fileTF.setText(selectedFile.getPath());
      }
    });
    databaseFile = fileTF.getText();
    openFilesButton.setDefaultButton(true);

    final Button submitButton = new Button("Submit File");
    submitButton.setOnAction(e -> {
      databaseManager.loadCsv(databaseFile, gameManager.getRound());
      jeopardyBoardPane = new JeopardyBoardPane(eventBus, gameManager.getRound());
      mainStage.setScene(getPlayerScene(mainStage));
    });
    submitButton.setDefaultButton(true);

    final HBox fileSelection = new HBox(20);
    fileSelection.setAlignment(Pos.CENTER);
    fileSelection.getChildren().addAll(openFiles, fileTF, openFilesButton);

    final VBox fileInputLayout = new VBox(20);
    fileInputLayout.setAlignment(Pos.CENTER);
    fileInputLayout.getChildren().addAll(instruction, fileSelection, submitButton);

    return new Scene(fileInputLayout, SCREEN_WIDTH, SCREEN_HEIGHT);
  }

  /**
   * Home screen to welcome player to game
   *
   * @param mainStage
   * @return home scene object
   */
  private Scene getHomeScene(final Stage mainStage) {
    final Label welcome = new Label("Welcome to Wheel of Jeopardy");
    welcome.setStyle(LARGER_FONT_SIZE);

    final Button playButton = new Button("Play Game with default questions");
    playButton.setOnAction(e -> {
      databaseFile = "questions-and-answers.csv";
      databaseManager.loadCsv(databaseFile, gameManager.getRound());
      jeopardyBoardPane = new JeopardyBoardPane(eventBus, gameManager.getRound());
      mainStage.setScene(getPlayerScene(mainStage));
    });
    playButton.setDefaultButton(true);
    playButton.setStyle(SMALL_FONT_SIZE);

    final Button fileInputButton = new Button("Play Game with custom questions");
    fileInputButton.setOnAction(e -> mainStage.setScene(getFileInputScene(mainStage)));
    fileInputButton.setStyle(SMALL_FONT_SIZE);

    final VBox homeLayout = new VBox(20);
    homeLayout.setAlignment(Pos.CENTER);
    homeLayout.getChildren().addAll(welcome, playButton, fileInputButton);

    return new Scene(homeLayout, SCREEN_WIDTH, SCREEN_HEIGHT);
  }

  private Scene getLoseTurnScene(final Stage mainStage, final Label categoryL) {
    final Button spinAgainButton = new Button("End Turn");
    spinAgainButton.setOnAction(e -> {
      gameManager.endCurrentPlayersTurn();
      mainStage.setScene(getWheelScene(mainStage));
    });
    spinAgainButton.setDefaultButton(true);

    // setup lose turn scene with the option of using a free turn
    final VBox loseTurnLayout = new VBox(20);
    loseTurnLayout.setAlignment(Pos.CENTER);
    loseTurnLayout.setStyle(SMALL_FONT_SIZE);

    if (currentQuestionPlayer.hasExtraTurns()) {
      final Button useFreeTurnButton = new Button("Use Free Turn");
      useFreeTurnButton.setOnAction(e -> {
        gameManager.getCurrentPlayer().removeExtraTurn();
        mainStage.setScene(getWheelScene(mainStage));
      });
      loseTurnLayout.getChildren().addAll(categoryL, spinAgainButton, useFreeTurnButton);
    } else {
      loseTurnLayout.getChildren().addAll(categoryL, spinAgainButton);
    }

    return new Scene(loseTurnLayout, SCREEN_WIDTH, SCREEN_HEIGHT);
  }

  private Scene getNewRoundScene(final Stage mainStage) {

    if (gameManager.getRound() == 1) {
      startNewRound = false;

      final GridPane rootNode = new GridPane();
      rootNode.setHgap(10);
      rootNode.setVgap(10);
      rootNode.setAlignment(Pos.CENTER);
      rootNode.setStyle(SMALL_FONT_SIZE);
      final Label roundOverMessage = new Label("Round 1 Has Ended!");
      final Label playerOneScoreMessage =
          new Label(String.format("%s score:\t%d", gameManager.getPlayers().get(0).getName(),
              gameManager.getPlayers().get(0).getRoundScore(1)));
      final Label playerTwoScoreMessage =
          new Label(String.format("%s score:\t%d", gameManager.getPlayers().get(1).getName(),
              gameManager.getPlayers().get(1).getRoundScore(1)));
      final Button startNewRoundButton = new Button("Start Round 2");
      startNewRoundButton.setAlignment(Pos.CENTER_LEFT);
      startNewRoundButton.setOnAction(e -> {
        gameManager.advanceToNextRound();
        databaseManager.loadCsv(databaseFile, gameManager.getRound());
        jeopardyBoardPane.dispose();
        jeopardyBoardPane = new JeopardyBoardPane(eventBus, gameManager.getRound());
        mainStage.setScene(getWheelScene(mainStage));
      });
      rootNode.add(roundOverMessage, 0, 0);
      rootNode.add(playerOneScoreMessage, 0, 1);
      rootNode.add(playerTwoScoreMessage, 0, 2);
      rootNode.add(startNewRoundButton, 0, 3);
      return new Scene(rootNode, SCREEN_WIDTH, SCREEN_HEIGHT);
    } else {
      return getEndGameScene(mainStage);
    }
  }

  /***
   * Player Scene to enter player names
   *
   * @param mainStage
   * @return player scene object
   */
  private Scene getPlayerScene(final Stage mainStage) {

    // create a grid pane layout
    final GridPane rootNode = new GridPane();
    rootNode.setHgap(10);
    rootNode.setVgap(10);
    rootNode.setAlignment(Pos.CENTER);
    rootNode.setStyle(SMALL_FONT_SIZE);

    // top label
    final Label enterPlayers = new Label("Please Add Players:");
    rootNode.add(enterPlayers, 0, 0);

    // add player 1
    final TextField player1Text = new TextField();
    player1Text.setAlignment(Pos.BASELINE_LEFT);
    rootNode.add(new Label("Player 1 Name: "), 0, 2);
    rootNode.add(player1Text, 1, 2);

    // add player 2
    final TextField player2Text = new TextField();
    player2Text.setAlignment(Pos.BASELINE_LEFT);
    rootNode.add(new Label("Player 2 Name: "), 0, 3);
    rootNode.add(player2Text, 1, 3);

    // enter button
    final Button playerButton = new Button("Save Players");
    playerButton.setOnAction(e -> {

      if (Strings.isNullOrEmpty(player1Text.getText())
          || Strings.isNullOrEmpty(player2Text.getText())) {
        final Alert alert =
            new Alert(AlertType.ERROR, "Player names must not be empty!", ButtonType.OK);
        alert.showAndWait();
        if (alert.getResult() == ButtonType.YES) {
          alert.close();
        }
      } else {
        // pass player names to GameManager
        gameManager.createPlayers(player1Text.getText(), player2Text.getText());
        mainStage.setScene(getWheelScene(mainStage));
      }
    });
    // button will be handle Enter key press automatically
    playerButton.setDefaultButton(true);
    rootNode.add(playerButton, 0, 5);

    // return player Scene
    return new Scene(rootNode, SCREEN_WIDTH, SCREEN_HEIGHT);
  }

  private Scene getPlayersChoiceScene(final String playerChoosing, final Stage mainStage) {
    final Label title = new Label(String.format("%s Landed on %s's Choice.",
        currentQuestionPlayer.getName(), playerChoosing));
    title.setStyle(SMALL_FONT_SIZE);

    final ComboBox<String> combo = new ComboBox<>();
    combo.setStyle(SMALL_FONT_SIZE);
    combo.getItems().addAll(WheelSectors.getJeopardyCategories());
    combo.getSelectionModel().select(0);

    final Label choosePrompt = new Label("Please select a category from the board above:");
    choosePrompt.setStyle(SMALL_FONT_SIZE);

    final Button askQuestionBtn = new Button("Ask Question");
    askQuestionBtn.setOnAction(e -> {
      // don't proceed unless a category is chosen.
      if (Strings.isNullOrEmpty(combo.getValue())) {
        final Alert alert = new Alert(AlertType.ERROR, "Please Select a Category!", ButtonType.OK);
        alert.showAndWait();
        if (alert.getResult() == ButtonType.YES) {
          alert.close();
        }
      } else {
        categorySelected = combo.getValue();
        eventBus.post(new RequestNextJeopardyQuestionScoreFromBoardMessage(categorySelected,
            gameManager.getRound()));
        mainStage.setScene(getQuestionScene(mainStage));
      }
    });
    askQuestionBtn.setDefaultButton(true);
    askQuestionBtn.setStyle(SMALL_FONT_SIZE);

    final GridPane rootPane = new GridPane();
    rootPane.setHgap(10);
    rootPane.setVgap(20);
    rootPane.setAlignment(Pos.CENTER);

    rootPane.add(title, 0, 0);
    rootPane.add(jeopardyBoardPane, 0, 1);
    rootPane.add(choosePrompt, 0, 2);
    rootPane.add(combo, 0, 3);
    rootPane.add(askQuestionBtn, 0, 4);

    return new Scene(rootPane, SCREEN_WIDTH, SCREEN_HEIGHT);

  }

  /**
   * Question scene to ask the player the question
   *
   * @param mainStage
   * @return question scene object
   */
  private Scene getQuestionScene(final Stage mainStage) {

    // display category, value, and question as returned by event bus
    final Label categoryL = new Label(String.format("%s landed on the category: %s",
        currentQuestionPlayer.getName(), categorySelected));

    if (questionAsked.isEmpty()) {

      // if lose turn
      if (categorySelected.equals(WheelSectors.getName(WheelSector.LOSE_TURN))) {
        return getLoseTurnScene(mainStage, categoryL);

      } else if (categorySelected.equals(WheelSectors.getName(WheelSector.PLAYERS_CHOICE))) {
        return getPlayersChoiceScene("Player", mainStage);
      } else if (categorySelected.equals(WheelSectors.getName(WheelSector.OPPONENTS_CHOICE))) {
        return getPlayersChoiceScene("Opponent", mainStage);
      } else {
        // spin button
        final Button spinAgainButton = new Button("End Turn");
        spinAgainButton.setOnAction(e -> mainStage.setScene(getWheelScene(mainStage)));
        spinAgainButton.setDefaultButton(true);
        // setup question scene
        final VBox questionLayout = new VBox(20);
        questionLayout.setAlignment(Pos.CENTER);
        questionLayout.setStyle(SMALL_FONT_SIZE);
        questionLayout.getChildren().addAll(categoryL, spinAgainButton);

        return new Scene(questionLayout, SCREEN_WIDTH, SCREEN_HEIGHT);
      }
    } else {
      final Label valueL = new Label("The next available question is for " + nextValue + " points");
      Label questionL = new Label("Question: " + questionAsked);
      final GridPane timeRemainingPane = new GridPane();
      final Label timeRemainingLabel =
          new Label(String.format("%d seconds remaining to answer question.", TIME_TO_ANSWER));
      final Timeline timeline = new Timeline();
      final KeyFrame keyFrame = new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
        int interval = TIME_TO_ANSWER;

        @Override
        public void handle(final ActionEvent event) {
          if (interval == 0) {
            timeline.stop();
            categoryL.setText("Timer expired. No points will be lost.");
            mainStage.setScene(getLoseTurnScene(mainStage, categoryL));
          }
          interval--;
          Platform.runLater(() -> timeRemainingLabel
              .setText(String.format("%d seconds remaining to answer question.", interval)));
        }



      });
      timeline.setCycleCount(Animation.INDEFINITE);
      timeline.getKeyFrames().add(keyFrame);
      timeline.playFromStart();
      final Button viewAnswerButton = new Button("View Answer");
      viewAnswerButton.setOnAction(e -> {
        timeline.stop();
        mainStage.setScene(getAnswerScene(mainStage));
      });
      viewAnswerButton.setDefaultButton(true);

      timeRemainingPane.add(new Label("Time remaining to answer question: "), 0, 0);
      timeRemainingPane.add(timeRemainingLabel, 1, 0);
      timeRemainingPane.setAlignment(Pos.CENTER);
      timeRemainingPane.setStyle(SMALL_FONT_SIZE);

      // setup question scene
      final VBox questionLayout = new VBox(20);
      questionLayout.setAlignment(Pos.CENTER);
      questionLayout.setStyle(SMALL_FONT_SIZE);
      if (questionAsked.endsWith(".jpg")) {
        questionL = new Label("Question: What is this a picture of?");
        final Image image = new Image(questionAsked);
        final ImageView iv1 = new ImageView();
        iv1.setImage(image);
        iv1.setFitHeight(500);
        iv1.setPreserveRatio(true);
        questionLayout.getChildren().addAll(categoryL, valueL, questionL, iv1, timeRemainingPane,
            viewAnswerButton);
      } else {
        questionLayout.getChildren().addAll(categoryL, valueL, questionL, timeRemainingPane,
            viewAnswerButton);
      }

      return new Scene(questionLayout, SCREEN_WIDTH, SCREEN_HEIGHT);
    }

  }

  private Scene getUseExtraTurnAfterIncorrectAnswerScene(final Stage mainStage,
      final PlayerAnsweredQuestionResultMessage msg) {

    final Label title = new Label("Would you like to use a free turn token?");
    final Button freeTurnButton = new Button("Yes");
    freeTurnButton.setOnAction(e -> {
      mainStage.setScene(getWheelScene(mainStage));
      gameManager.getCurrentPlayer().removeExtraTurn();
    });
    final Button doNotUseFreeTurnButton = new Button("No");
    doNotUseFreeTurnButton.setOnAction(e -> {
      eventBus.post(msg);
      gameManager.endCurrentPlayersTurn();
      mainStage.setScene(getWheelScene(mainStage));
    });
    doNotUseFreeTurnButton.setDefaultButton(true);
    // setup lose turn scene with the option of using a free turn
    final VBox questionLayout = new VBox(20);
    questionLayout.setAlignment(Pos.CENTER);
    questionLayout.setStyle(SMALL_FONT_SIZE);
    questionLayout.getChildren().addAll(title, freeTurnButton, doNotUseFreeTurnButton);

    return new Scene(questionLayout, SCREEN_WIDTH, SCREEN_HEIGHT);
  }

  /**
   * Wheel scene to see wheel and begin turn
   *
   * @param mainStage
   * @return Wheel scene object
   */
  private Scene getWheelScene(final Stage mainStage) {

    if (startNewRound) {
      return getNewRoundScene(mainStage);
    }
    // displayer round, players, scores, etc.
    final int round = gameManager.getRound();
    final int spinsRemaining = gameManager.getSpinsRemaining();
    final List<Player> players = gameManager.getPlayers();
    final Player player1 = players.get(0);
    final Player player2 = players.get(1);

    final Label roundNum = new Label("Round Number: " + Integer.toString(round));
    roundNum.setAlignment(Pos.CENTER_LEFT);
    roundNum.setStyle(SMALL_FONT_SIZE);

    final Label currPlayer =
        new Label(String.format("Current Player: %s", gameManager.getCurrentPlayer().getName()));
    currPlayer.setAlignment(Pos.CENTER_LEFT);
    currPlayer.setStyle(SMALL_FONT_SIZE);

    final Label spinsLeft =
        new Label(String.format("Spins Remaining: %s", Integer.toString(spinsRemaining)));
    spinsLeft.setAlignment(Pos.CENTER_LEFT);
    spinsLeft.setStyle(SMALL_FONT_SIZE);

    final Label player1Score =
        new Label(String.format("%s: %s points in round %s , total score %s", player1.getName(),
            player1.getRoundScore(round), Integer.toString(round), player1.getFinalScore()));
    player1Score.setStyle(SMALL_FONT_SIZE);

    final Label player2Score =
        new Label(String.format("%s: %s points in round %s , total score %s", player2.getName(),
            player2.getRoundScore(round), Integer.toString(round), player2.getFinalScore()));
    player2Score.setStyle(SMALL_FONT_SIZE);

    final Group wheel = createWheel();

    final RotateTransition rotateTransition = new RotateTransition(Duration.millis(2), wheel);
    rotateTransition.setFromAngle(0);
    rotateTransition.setToAngle(720);
    rotateTransition.setCycleCount(1);

    // view question button to see question after wheel spins
    final Button viewQuestionButton = new Button("View Result");
    viewQuestionButton.setStyle(LARGER_FONT_SIZE);
    viewQuestionButton.setOnAction(e -> mainStage.setScene(getQuestionScene(mainStage)));
    viewQuestionButton.setAlignment(Pos.CENTER);
    viewQuestionButton.setDisable(true);

    // spin button to sent bus notification for category selected
    final Button spinButton = new Button("Spin");
    spinButton.setStyle(LARGER_FONT_SIZE);
    spinButton.setOnAction(e -> {
      spinButton.setDefaultButton(false);
      spinButton.setDisable(true);
      rotateTransition.play();
      rotateTransition.setOnFinished(a -> {
        currentQuestionPlayer = gameManager.getCurrentPlayer();
        eventBus.post(new GuiPerformedSpinRequestMessage(currentQuestionPlayer));
        wheel.getChildren().add(indicateSelectedCategory());
        viewQuestionButton.setDisable(false);
        viewQuestionButton.setDefaultButton(true);
      });
    });
    spinButton.setAlignment(Pos.CENTER);
    spinButton.setDefaultButton(true);


    // setup wheel scene
    final HBox boardAndWheel = new HBox(20);
    boardAndWheel.setAlignment(Pos.CENTER);
    boardAndWheel.getChildren().addAll(jeopardyBoardPane, wheel);

    final VBox wheelLayout = new VBox(20);
    wheelLayout.setAlignment(Pos.CENTER);
    wheelLayout.getChildren().addAll(roundNum, spinsLeft, player1Score, player2Score, currPlayer,
        boardAndWheel, spinButton, viewQuestionButton);

    return new Scene(wheelLayout, SCREEN_WIDTH, SCREEN_HEIGHT);

  }

  /**
   * Indicate the selected wheel category with a green sector
   *
   * @param wheel
   */
  private Arc indicateSelectedCategory() {
    final float center = 750 / 2f;
    final float radius = 750 / 2f - 150;

    final List<WheelSector> categoryList = Arrays.asList(WheelSector.values());

    final Arc selector = new Arc();
    selector.setCenterX(center);
    selector.setCenterY(center);
    selector.setRadiusX(radius);
    selector.setRadiusY(radius);
    selector.setLength(30);

    selector.setFill(Color.TRANSPARENT);
    selector.setStroke(Color.GREEN);
    selector.setStrokeWidth(10);
    selector.setType(ArcType.ROUND);

    for (int count = 1; count <= 12; count++) {

      final WheelSector wheelSector = categoryList.get(count - 1);
      final String sectorName = WheelSectors.getName(wheelSector);

      if (categorySelected.equals(sectorName)) {
        final float startAngle = 180f - 30f * count;
        selector.setStartAngle(startAngle);
      }

    }
    return selector;
  }


  private void setPlayerAnsweredIncorrectlyScene(final Stage mainStage) {
    final PlayerAnsweredQuestionResultMessage msg =
        new PlayerAnsweredQuestionResultMessage(gameManager.getCurrentPlayer(), -1 * nextValue);

    if (gameManager.getCurrentPlayer().hasExtraTurns()) {
      mainStage.setScene(getUseExtraTurnAfterIncorrectAnswerScene(mainStage, msg));
    } else {
      eventBus.post(msg);
      gameManager.endCurrentPlayersTurn();
      mainStage.setScene(getWheelScene(mainStage));
    }
  }

}
