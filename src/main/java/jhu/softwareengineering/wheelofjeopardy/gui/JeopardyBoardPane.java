package jhu.softwareengineering.wheelofjeopardy.gui;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.EndRoundMessage;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.WheelSectors;

/**
 * Jeopardy Board JavaFX
 *
 * Muted sonarqube issue that GridPane inherits > 5 classes
 */
public class JeopardyBoardPane extends GridPane { // NOSONAR

  private final EventBus eventBus;
  Map<String, Map<Integer, Button>> jeopardySectors;

  public JeopardyBoardPane(final EventBus eventBus, final int round) {

    super();
    setHgap(10);
    setVgap(10);
    setAlignment(Pos.CENTER);
    setMouseTransparent(true);

    this.eventBus = eventBus;
    this.eventBus.register(this);
    jeopardySectors = new HashMap<>();
    final Integer[] catPrices = {100 * round, 200 * round, 300 * round, 400 * round, 500 * round};

    // Loop through categories and add them to the board
    int rowIndex = 0;
    int colIndex = 0;
    for (final String category : WheelSectors.getJeopardyCategories()) {
      final Button button = new Button(category);
      button.setPrefSize(100d, 40d);
      button.setStyle("-fx-background-color: #ffffff;" + "-fx-border-color: #000000; "
          + "-fx-border-width: 3px; " + "-fx-focus-color: transparent;");
      add(button, colIndex, rowIndex);
      // init maps inside of map
      final Map<Integer, Button> categoryMap = new LinkedHashMap<>();
      for (final int price : catPrices) {
        categoryMap.put(price, null);
      }
      jeopardySectors.put(category, categoryMap);
      colIndex++;
    }
    colIndex = 0;
    rowIndex = 1;
    // Add the individual price buttons to the map of maps
    for (final Integer price : catPrices) {
      for (final String category : WheelSectors.getJeopardyCategories()) {
        final Button button = new Button(price.toString());
        button.setStyle("-fx-background-color: #eeeeee;" + "-fx-border-color: #000000; "
            + "-fx-border-width: 2px; " + "-fx-focus-color: transparent;");
        button.setPrefSize(100d, 40d);
        add(button, colIndex, rowIndex);
        jeopardySectors.get(category).put(price, button);
        colIndex++;
      }
      colIndex = 0;
      rowIndex++;
    }
  }

  public void dispose() {
    eventBus.unregister(this);
  }

  @Subscribe
  public void handleGetNextQuestionScoreMessage(
      final RequestNextJeopardyQuestionScoreFromBoardMessage request) {
    // disable the next score in the requested category
    int score = -1;
    final String category = request.getCategory();
    final Map<Integer, Button> categoryButtons = jeopardySectors.get(category);
    for (final Entry<Integer, Button> entry : categoryButtons.entrySet()) {
      if (!entry.getValue().isDisabled()) {
        score = entry.getKey();
        entry.getValue().setDisable(true);
        if (entry.getKey() == 500 * request.getRound()) {
          WheelSectors.removeCategory(category);
          if (WheelSectors.getJeopardyCategories().isEmpty()) {
            // no categories left, send end round msg
            final EndRoundMessage msg = new EndRoundMessage(request.getRound());
            eventBus.post(msg);
          }
        }
        break;
      }
    }
    eventBus.post(new RequestJeopardyQuestionFromDatabaseMessage(request.getCategory(), score,
        request.getRound()));
  }

}
