package jhu.softwareengineering.wheelofjeopardy.gui;

import jhu.softwareengineering.wheelofjeopardy.databasemanager.JeopardyQuestion;

/**
 * Message class sent from the DatabaseManager to the GUI containing the JeopardyQuestion.
 */
public class JeopardyQuestionResponseToGuiMessage {

  private final JeopardyQuestion jeopardyQuestion;

  public JeopardyQuestionResponseToGuiMessage(final JeopardyQuestion jeopardyQuestion) {
    this.jeopardyQuestion = jeopardyQuestion;
  }

  public JeopardyQuestion getJeopardyQuestion() {
    return jeopardyQuestion;
  }

}
