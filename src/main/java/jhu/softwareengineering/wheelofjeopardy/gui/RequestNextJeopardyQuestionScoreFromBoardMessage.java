package jhu.softwareengineering.wheelofjeopardy.gui;

public class RequestNextJeopardyQuestionScoreFromBoardMessage {

  private final String category;
  private final int round;

  public RequestNextJeopardyQuestionScoreFromBoardMessage(final String category, final int round) {
    this.category = category;
    this.round = round;
  }

  public String getCategory() {
    return category;
  }

  public int getRound() {
    return round;
  }
}
