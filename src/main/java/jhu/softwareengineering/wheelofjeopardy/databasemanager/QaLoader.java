package jhu.softwareengineering.wheelofjeopardy.databasemanager;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

/**
 * Utility class that creates an embedded Java H2 database. This class will contain all of the
 * necessary database functions to interact with the H2 database in order to store and retrieve
 * various persistent data.
 *
 * @author Adam
 *
 */
public class QaLoader {

  /*
   * Connection string for the H2 database. The DB_CLOSE_DELAY bit prevents the connection from
   * closing and re-opening constantly which dramatically slows down back-to-back database
   * operations.
   */
  private static final String CONNECTION_STRING = "jdbc:h2:~/test;DB_CLOSE_DELAY=-1";
  private static final Logger LOGGER = LogManager.getLogger(QaLoader.class);
  private static final String USER = "";
  private int insertCount = 0;
  private String pass;

  QaLoader() {
    try {
      pass = DbConnection.loadEncryptedPassword();
    } catch (final IOException e) {
      LOGGER.error("Unable to load datasource password.", e);
    }
  }

  public boolean insertQuestionAnswer(final String question, final String answer,
      final String category, final int pointValue, final int round) {
    final String sql = "INSERT INTO " + DbConnection.TABLE_NAME
        + " (question, answer, category, pointvalue, round) values(?,?,?,?,?)";
    try (final Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, pass);
        final PreparedStatement statement = conn.prepareStatement(sql)) {

      statement.setString(1, question); // 1 specifies first parameter
      statement.setString(2, answer); // 2 specifies second parameter
      statement.setString(3, category); // 3 specifies third parameter
      statement.setInt(4, pointValue); // 4 specifies fourth parameter
      statement.setInt(5, round);
      statement.executeUpdate();
      insertCount++;
      LOGGER.info("Inserted question number {} into database.", insertCount);
      return true;
    } catch (final SQLException sqlException) {
      LOGGER.error("Error inserting into database.", sqlException);
      return false;
    }
  }



  public void parseCsv(final String csvPath, final DbConnection dbConnection) {

    final DbConnection db = dbConnection;

    /* make sure we start the database with a clean and consistent state */
    db.dropQuestionAnswerTable();
    db.deleteH2Db();

    /* create the questions table */
    db.createQuestionTable();

    /* load the questions/answers/categories/scores from csv */
    final ClassLoader classLoader = getClass().getClassLoader();
    final File csvFile;
    if (classLoader.getResource(csvPath) != null) {
      csvFile = new File(classLoader.getResource(csvPath).getFile());
    } else {
      csvFile = new File(csvPath);
    }

    final Path csvFilePath = Paths.get(csvFile.toURI());
    try (Reader reader = Files.newBufferedReader(csvFilePath);
        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build()) {
      String[] nextRecord;
      while ((nextRecord = csvReader.readNext()) != null) {
        insertQuestionAnswer(nextRecord[0].trim(), nextRecord[1].trim(), nextRecord[2].trim(),
            Integer.parseInt(nextRecord[3].trim()), Integer.parseInt(nextRecord[4].trim()));
      }

    } catch (final Exception e) {
      LOGGER.info("Unable to parse csv file.", e);
    }
  }

}
