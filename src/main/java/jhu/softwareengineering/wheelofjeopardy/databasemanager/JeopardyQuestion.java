package jhu.softwareengineering.wheelofjeopardy.databasemanager;

/**
 * Data structure to hold the information pertaining to a question from the Jeopardy board.
 */
public class JeopardyQuestion {

  private final String answer;
  private final String category;
  private final int pointValue;
  private final String question;
  private final int round;

  public JeopardyQuestion(final String question, final String answer, final int pointValue,
      final String category, final int round) {
    this.question = question;
    this.answer = answer;
    this.pointValue = pointValue;
    this.category = category;
    this.round = round;
  }

  public String getAnswer() {
    return answer;
  }

  public String getCategory() {
    return category;
  }

  public int getPointValue() {
    return pointValue;
  }

  public String getQuestion() {
    return question;
  }

  public int getRound() {
    return round;
  }

}
