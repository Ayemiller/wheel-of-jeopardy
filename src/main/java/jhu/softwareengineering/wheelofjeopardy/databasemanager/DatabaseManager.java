package jhu.softwareengineering.wheelofjeopardy.databasemanager;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.WheelSectors;
import jhu.softwareengineering.wheelofjeopardy.gamemanager.WheelSectors.WheelSector;
import jhu.softwareengineering.wheelofjeopardy.gui.JeopardyQuestionResponseToGuiMessage;
import jhu.softwareengineering.wheelofjeopardy.gui.RequestJeopardyQuestionFromDatabaseMessage;

/**
 * Question and Answer service that inserts a bunch of mock data into the question/answer table of
 * the H2 database. Eventually we can use this to initialize the database with actual questions and
 * answers.
 *
 * @author Adam
 *
 */
public class DatabaseManager {

  private static final Logger LOGGER = LogManager.getLogger(DatabaseManager.class);
  private final EventBus eventBus;
  DbConnection dbConnection;

  public DatabaseManager(final EventBus eventBus) {
    this.eventBus = eventBus;
    this.eventBus.register(this);
    dbConnection = new DbConnection();
  }

  @Subscribe
  /**
   * Request made from the GUI to the DatabaseManager when the GUI is requesting a Jeopardy
   * question.
   *
   * @param request
   */
  public void handleQuestionRequest(final RequestJeopardyQuestionFromDatabaseMessage request) {
    final JeopardyQuestion question = dbConnection.getQuestionFromCategory(request.getCategory(),
        request.getPointValue(), request.getRound());
    final JeopardyQuestionResponseToGuiMessage response =
        new JeopardyQuestionResponseToGuiMessage(question);
    eventBus.post(response);
  }

  public void loadCsv(final String csvPath, final int round) {
    startUp(csvPath, round);
  }

  /**
   * Sets the {@link WheelSectors} for the categories based on the questions in the database
   */
  private void setCategories(final int round) {


    final List<String> categoriesFromDb = dbConnection.getQuestionCategories(round);

    WheelSectors.setName(WheelSector.CATEGORY_1, categoriesFromDb.get(0));
    LOGGER.info("Category 1 set to {}", categoriesFromDb.get(0));
    WheelSectors.setName(WheelSector.CATEGORY_2, categoriesFromDb.get(1));
    LOGGER.info("Category 2 set to {}", categoriesFromDb.get(1));
    WheelSectors.setName(WheelSector.CATEGORY_3, categoriesFromDb.get(2));
    LOGGER.info("Category 3 set to {}", categoriesFromDb.get(2));
    WheelSectors.setName(WheelSector.CATEGORY_4, categoriesFromDb.get(3));
    LOGGER.info("Category 4 set to {}", categoriesFromDb.get(3));
    WheelSectors.setName(WheelSector.CATEGORY_5, categoriesFromDb.get(4));
    LOGGER.info("Category 5 set to {}", categoriesFromDb.get(4));
    WheelSectors.setName(WheelSector.CATEGORY_6, categoriesFromDb.get(5));
    LOGGER.info("Category 6 set to {}", categoriesFromDb.get(5));
  }

  /**
   * Loads the CSV information into the H2 database.
   */
  protected void startUp(final String csvPath, final int round) {
    LOGGER.info("Running the question and answer service.");
    final QaLoader qaLoader = new QaLoader();
    qaLoader.parseCsv(csvPath, dbConnection);
    setCategories(round);
  }

}
