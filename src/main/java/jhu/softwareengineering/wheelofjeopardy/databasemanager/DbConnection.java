package jhu.softwareengineering.wheelofjeopardy.databasemanager;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.tools.DeleteDbFiles;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.iv.RandomIvGenerator;
import org.jasypt.properties.EncryptableProperties;

/**
 * Utility class that creates an embedded Java H2 database. This class will contain all of the
 * necessary database functions to interact with the H2 database in order to store and retrieve
 * various persistent data.
 *
 * @author Adam
 *
 */
public class DbConnection {

  public static final String TABLE_NAME = "questions";
  private static final String ANSWER_COLUMN = "answer";
  private static final String CATEGORY_COLUMN = "category";
  /*
   * Connection string for the H2 database. The DB_CLOSE_DELAY bit prevents the connection from
   * closing and re-opening constantly which dramatically slows down back-to-back database
   * operations.
   */
  private static final String CONNECTION_STRING = "jdbc:h2:~/test;DB_CLOSE_DELAY=-1";
  private static final Logger LOGGER = LogManager.getLogger(DbConnection.class);
  private static final String ORDER_BY_ID_ASC = " ORDER BY id ASC";
  private static final String QUESTION_COLUMN = "question";
  private static final String ROUND_COLUMN = "round";
  private static final String SELECT_ERROR_MSG = "Error running select clause.";
  private static final String USER = "";

  public static String loadEncryptedPassword() throws IOException {
    final StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
    encryptor.setPassword("pwdGenPassphrase"); // could be got from web, env variable...
    encryptor.setAlgorithm("PBEWithHMACSHA512AndAES_256");
    encryptor.setIvGenerator(new RandomIvGenerator());

    final Properties props = new EncryptableProperties(encryptor);
    props.load(new FileInputStream("src/main/resources/secure.properties"));
    return props.getProperty("datasource.filepassword") + " "
        + props.getProperty("datasource.userpassword");
  }

  private String secret;

  DbConnection() {
    try {
      secret = loadEncryptedPassword();
    } catch (final IOException e) {
      LOGGER.error("Unable to load datasource password.", e);
    }
  }

  /**
   * Creates the H2 database table containing Jeopardy questions.
   *
   * @return true if success, false otherwise
   */
  public boolean createQuestionTable() {
    LOGGER.info("Creating the question table.");
    final String sql = "create table " + TABLE_NAME
        + " (id bigint auto_increment primary key, question varchar(255), answer varchar(255), category varchar(255), pointvalue int, round int)";
    try (final Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, secret);
        final Statement statement = conn.createStatement()) {
      statement.execute(sql);
      return true;
    } catch (final SQLException sqlException) {
      LOGGER.error("Error creating question & answer table.", sqlException);
      return false;
    }
  }

  /**
   * Absolutely delete all H2 database files
   */
  public void deleteH2Db() {
    DeleteDbFiles.execute("~", "test", true);
  }

  /**
   * Drops the H2 database table containing Jeopardy questions.
   *
   * @return true if success, false otherwise
   */
  public boolean dropQuestionAnswerTable() {
    LOGGER.info("Clearing " + TABLE_NAME + " table.");
    try (final Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, secret);
        final Statement statement = conn.createStatement()) {
      statement.execute("drop table " + TABLE_NAME);
      return true;
    } catch (final SQLException sqlException) {
      LOGGER.debug("Error clearing {} table. Perhaps it hasn't been created?", TABLE_NAME,
          sqlException);
      return false;
    }
  }

  /**
   * Returns a list containing {@link JeopardyQuestion} objects contained in the database.
   *
   * @return
   */
  public List<JeopardyQuestion> getAllJeopardyQuestion() {
    final String sql = "SELECT id, category, question, answer, pointvalue, round FROM " + TABLE_NAME
        + ORDER_BY_ID_ASC;
    final List<JeopardyQuestion> questions = new ArrayList<>();
    try (final Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, secret);
        final Statement statement = conn.createStatement();

        final ResultSet rs = statement.executeQuery(sql)) {
      while (rs.next()) {
        final JeopardyQuestion question =
            new JeopardyQuestion(rs.getString(QUESTION_COLUMN), rs.getString(ANSWER_COLUMN),
                rs.getInt("pointvalue"), rs.getString(CATEGORY_COLUMN), rs.getInt(ROUND_COLUMN));
        questions.add(question);
      }
      return questions;
    } catch (final SQLException sqlException) {
      LOGGER.error(SELECT_ERROR_MSG, sqlException);
      return new ArrayList<>();
    }
  }

  /**
   * Retrieves a Jeopardy questions from the database from a specified category and a specified
   * point value.
   *
   * @param category
   * @param pointValue
   * @return
   */
  public JeopardyQuestion getQuestionFromCategory(final String category, final int pointValue,
      final int round) {
    final String sql = "SELECT id, category, question, answer, pointvalue, round FROM " + TABLE_NAME
        + " WHERE category=? AND pointvalue=? AND round=?" + ORDER_BY_ID_ASC;
    try (final Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, secret);
        final PreparedStatement prepStatement =
            getPreparedStatement(conn, sql, category, pointValue, round);
        final ResultSet rs = prepStatement.executeQuery()) {
      if (rs.next()) {
        return new JeopardyQuestion(rs.getString(QUESTION_COLUMN), rs.getString(ANSWER_COLUMN),
            rs.getInt("pointvalue"), rs.getString(CATEGORY_COLUMN), rs.getInt(ROUND_COLUMN));
      }
    } catch (final SQLException sqlException) {
      LOGGER.error(SELECT_ERROR_MSG, sqlException);
    }
    return null;
  }

  /**
   * Prints all of the questions and answers that reside in the database.
   *
   * @return
   */
  public boolean printQuestionAnswers() {
    LOGGER.info("Printing contents of " + TABLE_NAME + " table:");
    final String sql = "SELECT id, question, answer, category, pointvalue, round FROM " + TABLE_NAME
        + ORDER_BY_ID_ASC;
    try (final Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, secret);
        final Statement statement = conn.createStatement();
        final ResultSet rs = statement.executeQuery(sql)) {
      while (rs.next()) {
        LOGGER.info("id: {} category: {} question: {} answer: {} round: {}", rs.getInt("id"),
            rs.getString(CATEGORY_COLUMN), rs.getString(QUESTION_COLUMN),
            rs.getString(ANSWER_COLUMN), rs.getInt(ROUND_COLUMN));
      }
      return true;
    } catch (final SQLException sqlException) {
      LOGGER.error(SELECT_ERROR_MSG, sqlException);
      return false;
    }
  }

  private PreparedStatement getPreparedStatement(final Connection conn, final Object... args)
      throws SQLException {
    /*
     * this prepared statement gets passed back to the caller and is closed using a
     * try-with-resources statement, so we don't close it here
     */
    final PreparedStatement ps = conn.prepareStatement((String) args[0]); // NOSONAR
    for (int i = 1; i < args.length; i++) {
      ps.setObject(i, args[i]);
    }

    return ps;

  }

  /**
   * Returns a list of the Jeopardy questions contained in the database.
   *
   * @return list of Jeopardy categories as defined by the contents of the database
   */
  List<String> getQuestionCategories(final int round) {
    final String sql = "SELECT category FROM " + TABLE_NAME
        + " WHERE round=? GROUP BY CATEGORY ORDER BY CATEGORY ASC";
    final List<String> categories = new ArrayList<>();
    try (final Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, secret);
        final PreparedStatement prepStatement = getPreparedStatement(conn, sql, round);
        final ResultSet rs = prepStatement.executeQuery()) {
      while (rs.next()) {
        categories.add(rs.getString(CATEGORY_COLUMN));
      }
      return categories;
    } catch (final SQLException e) {
      LOGGER.error("Unable to load categories from database.", e);
      return new ArrayList<>();
    }
  }

}
