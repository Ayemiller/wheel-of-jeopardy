# Wheel of Jeopardy: Game of Threads Edition

## Requirements to Run

  - Oracle JDK 8 (due to JavaFX)
  - Tested on Windows 10, Mac OS Mojave, Red Hat 7

## How to Create Your Own Questions/Answers

 - View the CSV file questions-and-answers.csv located in src/main/resources.
 
 - Create a new CSV file with the same header:
  ```question, answer, category, scores, round```

 - Add 60 questions total:

     - 5 Questions per category

     - 6 Categories per round

     - 2 Rounds

     - 5 * 6 * 2 = 60 questions

 - Save the CSV file anywhere on your computer. The GUI will let you import the CSV file via file browser when you click ```Play Game with custom questions```

## Git Basics

### Cloning the project

#### Run the following command in a terminal

##### ```git clone https://YOUR-USERNAME@bitbucket.org/Ayemiller/wheel-of-jeopardy.git```

### Create a Branch

#### Run the following command in a terminal

##### For a feature branch

```git checkout -b feature/my-feature-name```

##### For a bugfix branch

```git checkout -b bugfix/bugfix-description```

##### For a refactor branch

```git checkout -b refactor/what-is-being-refactored```

### Committing to your local branch

#### Staging files to be committed

##### ```git add path/to/file```

#### Performing the commit

##### ```git commit -m "your commit message"```

### Merging the latest from the master branch into your branch

#### Run the following command in a terminal

##### ```git merge origin/master```

### Pushing your branch to the BitBucket server

#### Run the following command in a terminal

##### ```git push origin your-branch-name```

#### Getting rid of unwanted changes in your curent workspace

##### Least Destructive - Reset a single file

```git checkout -- path/to/file```

##### Not-very-dangerous method

###### Stash the commit

```git stash```

###### Restore what you've stashed

```git stash pop```

##### More dangerous method (just get rid of your changes)

```git reset --hard```

##### Most dangerous method (git rid of everything not tracked by Git, really everything)

```git clean -dfx```

### If You Really Screwed Up...Good Git Reference

[Oh Sh** Git](https://ohshitgit.com)

## Gradle Background Info

### The project uses the gradle build tool.  This downloads all dependencies automatically, generates IDE project files automatically, and gives us a platform independent way to build and run the project.

## Running the Program

### Linux/Mac: run this command from the project root ```./gradlew run```

### Windows Command Prompt: run this command from the project root ```gradlew.bat run```

### Windows PowerShell: run this command from the project root ```./gradlew.bat run```

## Running Unit Tests

### Linux/Mac: run this command from the project root ```./gradlew test```

### Windows Command Prompt: run this command from the project root ```gradlew.bat test```

### Windows PowerShell: run this command from the project root ```./gradlew.bat test```

## Running Sonarqube Analysis

### Linux/Mac: run this command from the project root ```./gradlew sonarqube```

### Windows Command Prompt: run this command from the project root ```gradlew.bat sonarqube```

### Windows PowerShell: run this command from the project root ```./gradlew.bat sonarqube```

### To view the analysis, go to https://sonarcloud.io and log in with your Bitbucket account

## Creating Eclipse Project Files

### It's important to re-run these commands when the build.gradle file changes or when source files are modified outside of Eclipse.  Eclipse is kind of dumb about recognizing file system changes. After running the below commands, make sure to select all files in the Package Explorer and press F5 or right-click->refresh.

### Linux/Mac: run ```./gradlew cleanEclipse eclipse```

### Windows Command Prompt: run ```gradlew.bat cleanEclipse eclipse```

### Windows PowerShell: run ```./gradlew.bat cleanEclipse eclipse```

### Running in Eclipse

- Right click on Package Manager
- Select Import->General->Existing Projects into Workspace
- Browse to the root of this project, where the build.gradle file is located
- Make sure the wheelofjeopardy project is selected and click OK.
- In the Package Explorer, navigate to jhu.softwareengineering.wheelofjeopardy.gui.WheelOfJeopardyStage, right click, and select Run As -> Java Application.
