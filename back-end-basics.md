# Backend

## Jeopardy Board Service

* Question Generation
    * Randomly select question

* Answer Generation
    * Select answer given question

## Wheel Service

* Sector Generation
    * Randomly select sector from datastore
* Sector Rectifier
    * Map wheel sector to jeopardy category


## Scoring Service

* User Scoring Service
    * Add points for correct answer
    * Subtract points for incorrect answer
* Judge Service
    * Get answer
        * Store oral answer
    * Grade answer
        * Get opponent concensus
        * Calculate grade
    * Get grade

## Gameplay Manager

  * Round Service
      * Start round
      * End round
      * Get round
      * Advance round
  * Turn Service
      * Start turn
      * End turn
      * Ask question
          * Add points
          * Subtract points
      * Lose turn
      * Free turn
      * Bankrupt
      * Player's choice
      * Opponent's choice
      * Double your score

  * Timing Service
      * Start timer
      * End timer

## Admin Interface

* Add questions
* Edit questions
* Delete questions
* Add answers
* Edit answers
* Delete answers
